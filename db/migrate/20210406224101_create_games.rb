class CreateGames < ActiveRecord::Migration[6.1]
  def change
    create_table :games do |t|
      t.string :status, :default => 'in_progress'
      t.string :turn, :default => 'X'

      t.timestamps
    end
  end
end
