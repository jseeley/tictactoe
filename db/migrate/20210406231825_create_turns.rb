class CreateTurns < ActiveRecord::Migration[6.1]
  def change
    create_table :turns do |t|
      t.references :game, null: false, foreign_key: true
      t.string :player
      t.integer :tile

      t.timestamps
    end
  end
end
