require 'rails_helper'

RSpec.describe "games/index", type: :view do
  before(:each) do
    assign(:games, [
      Game.create!(
        status: "Status",
        turn: "Turn"
      ),
      Game.create!(
        status: "Status",
        turn: "Turn"
      )
    ])
  end

  it "renders a list of games" do
    render
    assert_select "tr>td", text: "Status".to_s, count: 2
    assert_select "tr>td", text: "Turn".to_s, count: 2
  end
end
