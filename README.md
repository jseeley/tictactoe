# README

1. Clone this project
2. cd into the tictactoe directory
3. run `rails webpacker:install`
4. run `rails s` to start the server
5. visit localhost:3000 to try out the application

* I checked in the development.sqlite database (something I wouldn't
typically do) to hopefully make it quicker/easier to get running and
test out.

# Notes

There are no tests and there is no validation middleware.
I started writing implementation code first (instead of TDD) and tried to
stick to the two hours specified and I ran out of time to put in
validations, add tests, and re-factor this so that it isn't such an ugly
mess.  I hope you'll forgive me.

All the best,
          Joiey
