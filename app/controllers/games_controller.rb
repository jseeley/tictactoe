class GamesController < ApplicationController
  before_action :set_game, only: %i[ show edit update destroy ]
  before_action :accept_all_params

  # GET /games
  def index
    @games = Game.all
  end

  # GET /games/1
  def show
  end

  # GET /games/new
  def new
    @game = Game.new

    respond_to do |format|
      if @game.save
        format.html { redirect_to @game, notice: "Game was successfully created." }
      else
        # TODO: replace with a generic error page
        format.html { render :new, status: :unprocessable_entity }
      end
    end
  end

  # GET /games/1/edit
  # def edit
  # end

  # POST /games
  # def create
  #   @game = Game.new(game_params)

  #   respond_to do |format|
  #     if @game.save
  #       format.html { redirect_to @game, notice: "Game was successfully created." }
  #       format.json { render :show, status: :created, location: @game }
  #     else
  #       format.html { render :new, status: :unprocessable_entity }
  #       format.json { render json: @game.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  def save
    @game = Game.find(params[:id])

    tile = []
    tiles = ['h0', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'h7', 'h8']
    params.each do |key, val|
      if tiles.include? key
        unless val.empty?
          tile << key[-1]
        end
      end
    end

    if tile.length > 1
      # TODO: Should do better than this...
      raise "You can't take more than one turn at a time."
    end

    turn = Turn.new(tile: tile[0], player: @game.turn, game_id: @game.id)
    turn.save

    total_turns = Turn.select(:tile).where(game_id: @game.id).length
    player_turns = Turn.select(:tile).where(game_id: @game.id, player: @game.turn).order(:tile).map(&:tile)

    if(player_turns.length >= 3)
      Game.winning_combos.each do |combo|
        diff = combo - player_turns
        if diff.length == 0
          message = 'Player ' + @game.turn + ' has won the game!'
          @game.status = message
          @game.save
          redirect_to @game, notice: message
          return
        end
      end
    end

    if total_turns >= 9 # Should never actually be greater than 9...
      message = 'The game has ended in a draw :('
      @game.status = message
      @game.save
      redirect_to @game, notice: message
      return
    end

    @game.turn = @game.turn == 'X' ? 'O' : 'X'
    @game.save

    redirect_to @game, notice: "Game was successfully updated."
    return
  end

  # PATCH/PUT /games/1
  def update
    byebug

    respond_to do |format|
      if @game.update(game_params)
        format.html { redirect_to @game, notice: "Game was successfully updated." }
      else
        format.html { render :edit, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /games/1
  def destroy
    @game.destroy
    respond_to do |format|
      format.html { redirect_to games_url, notice: "Game was successfully destroyed." }
      # format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_game
      @game = Game.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def game_params
      params.require(:game).permit(:status, :turn)
    end

    def turn_params
      params.require(:turn).permit(:h0, :h1, :h2, :h3, :h4, :h5, :h6, :h7, :h8)
    end

    def accept_all_params
      params.permit!
    end
end
