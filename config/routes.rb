Rails.application.routes.draw do
  root 'games#index'
  resources :games
  post "/games/save", to: "games#save"
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
